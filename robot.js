const mongoose = require('mongoose');
const Hotel = require('./models/hotel.js');
const fillDatabaseWithHotelData = require('./schedules-hotel.js');
const fillDatabaseWithFlyOfferData = require('./schedules-fly.js');

//connect to database
mongoose.connect(process.env.MONGO).then(() => console.log('database connected')).catch(err => { console.log(err) });

//destination
const flyInteraries = [
    {"dep":"PAR","dest":"ROM"},
    {"dep":"ROM","dest":"PAR"},
    {"dep":"PAR","dest":"BER"},
    {"dep":"BER","dest":"PAR"},
    {"dep":"ROM","dest":"BER"},
    {"dep":"BER","dest":"ROM"}
]
// cron.schedule(' */15 * * * * *', async () => {
//
async function main(){
    console.log("robot script running")
    let d = new Date();
    let monthInDate = d.getMonth()+1;
    let dayNumberInDate = d.getDate();
    let monthInDateToString ="";
    if(monthInDate<10){
        monthInDateToString="0"+monthInDate
        }else{
            monthInDateToString = monthInDate
            }
            let dayNumberInDateToString = "";
            if(dayNumberInDate<10){
                dayNumberInDateToString="0"+dayNumberInDate
                }else{
                    dayNumberInDateToString = dayNumberInDate
                    }
    const checkIntDate = d.getFullYear() + "-" + monthInDateToString + "-" + dayNumberInDateToString;
    const dateOut = new Date();
    dateOut.setDate(dateOut.getDate() + 7);
    let monthOutDate = dateOut.getMonth()+1;
    let dayNumberOutDate = dateOut.getDate();
    let monthOutDateToString ="";
    if(monthOutDate<10){
        monthOutDateToString="0"+monthOutDate
        }else{
            monthOutDateToString = monthOutDate
            }
            let dayNumberOutDateToString = "";
            if(dayNumberOutDate<10){
                dayNumberOutDateToString="0"+dayNumberOutDate
                }else{
                    dayNumberOutDateToString = dayNumberOutDate
                    }
    const checkOutDate = dateOut.getFullYear() + "-" + monthOutDateToString + "-" + dayNumberOutDateToString;
    await Hotel.deleteMany({});
    await fillDatabaseWithHotelData(checkIntDate, checkOutDate, 1);
    await fillDatabaseWithHotelData(checkIntDate, checkOutDate, 2);
    await fillDatabaseWithHotelData(checkIntDate, checkOutDate, 3);
    for(let it of flyInteraries){
        setTimeout(() => console.log("Wait 100ms"), 200);
        await fillDatabaseWithFlyOfferData(it.dep, it.dest);
    }
    await mongoose.disconnect();
}

main().catch((err) => console.log(err));




