const cityCodes = require("./models/cityCodes");
const Fly = require("./models/fly.js");
const amadeus = require("./service/amadeus.js");
const { deleteMany } = require("./models/cityCodes");


async function fillDatabaseWithFlyOfferData(origin, destination) {
  //await data 
  let response = await findFlyOffer(origin, destination);
  //return response
  let datas = response.data;
  await Fly.deleteMany({});
  for (let data of datas) {

    let its = data.itineraries[0].segments;
    const dep = its[0].departure.iataCode
    let dest = "";
    if (its.length == 1) {
      dest = its[0].arrival.iataCode
    } else if (its.length > 1) {
      const i = its.length
      dest = its[i - 1].arrival.iataCode
    }
    let fly = {
      numberOfBookableSeats: data.numberOfBookableSeats, // String is shorthand for {type: String}
      flyStart: data.itineraries[0].segments[0],
      flyArrival: data.itineraries[0].segments[1],
      duration: data.itineraries[0].duration,
      price: data.price,
      dept: dep,
      dest: dest
    };
    //record flies
    await testSave(fly);
  }
}

function testSave(flyEntitie) {
  return new Promise((resolve, reject) => {
    let fly = new Fly(flyEntitie);
    fly.save(function (err, savedEntity) {
      if (err != null) {
        console.log(err);
        reject({ error: "error", cause: err });
      } else {
        
        flyEntitie.id = savedEntity.id;
        resolve(flyEntitie);
      }
    });
  });
}

async function findFlyOffer(origin, destination) {
  let d = new Date();
  let monthInDateToString ="";
  let monthInDate = d.getMonth()+1;
  let dayNumberInDate = d.getDate();
    if(monthInDate<10){
        monthInDateToString="0"+monthInDate
        }else{
            monthInDateToString = monthInDate
            }
            let dayNumberInDateToString = "";
            if(dayNumberInDate<10){
                dayNumberInDateToString="0"+dayNumberInDate
                }else{
                    dayNumberInDateToString = dayNumberInDate
                    }
    const departureDate = d.getFullYear() + "-" + monthInDateToString + "-" + dayNumberInDateToString;
    let flyOffer = await amadeus.shopping.flightOffersSearch.get({
    originLocationCode: origin,
    destinationLocationCode: destination,
    departureDate: departureDate,
    //departureDate: "2022-12-21",
    adults: "2",
  });
  // console.log(hotels);
  return flyOffer;
}
module.exports = fillDatabaseWithFlyOfferData;
