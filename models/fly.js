const mongoose = require('mongoose');

const flySchema = new mongoose.Schema({
    numberOfBookableSeats:  Number, // String is shorthand for {type: String}
    dept: String,
    dest:String,
    duration: String,
    flyStart: {departure: {iataCode: String, at: Date},duration: String, arrival: {iataCode: String, terminal: String, at: Date}},
    flyArrival: {departure: {iataCode: String, at: Date},duration: String, arrival: {iataCode: String, terminal: String, at: Date}},
    // itineraries: [{duration: String, segments:[{departure: {iataCode: String, at: String},duration: String, arrival: {iataCode: String, terminal: String, at: String}}]}],
    price: { base: Number, total: Number, currency: String},
    date: { type: Date, default: Date.now },
    
    }
  );
  module.exports = mongoose.model("Fly", flySchema);

