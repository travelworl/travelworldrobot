const mongoose = require('mongoose');

const hotelSchema = new mongoose.Schema({
    name:  String, // String is shorthand for {type: String}
    dupeId: String,
    cityCode:String,
    adresse: { postalCode: String, cityName : String , countryCode : String, lines: [String]},
    date: { type: Date, default: Date.now },
    hidden: Boolean,
    contact: {
      phone: String,
      fax:  String,
      email: String
    },
    offers:[{
      checkInDate : {type: Date},
      checkOutDate :  {type: Date},
      guests : Number,
      price : {
        base: Number, total: Number, currency: String
      }
    }]
  });
  module.exports = mongoose.model("Hotel", hotelSchema);

