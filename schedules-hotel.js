
const cityCodes = require('./models/cityCodes');
const Hotel = require('./models/hotel.js');
const amadeus = require('./service/amadeus.js');


async function fillDatabaseWithHotelData(checIntDate,checkOutDate, guests) {
  let result = await cityCodes.find();
 
  for (key in result) {
    console.log("log result key",result[key].code);
    let response = await findHotelByCityCodes(result[key].code, checIntDate, checkOutDate, guests);
   
    let datas = response.data;
    console.log("log of data",datas)
    for (let data of datas) {
      let hotelEntitie = {
        "name": data.hotel.name,
        "dupeId": data.hotel.dupeId,
        "hidden": data.available,
        "contact": data.hotel.contact,
        "adresse": data.hotel.address,
        "cityCode": data.hotel.cityCode
      };
      let offers  =[]
      for(offer of data.offers){
          
          offer = {
          "checkInDate" :offer.checkInDate,
          "checkOutDate" : offer.checkOutDate,
          "guests": offer.guests.adults,
          "price": {
            "base": Number(offer.price.base != undefined ? offer.price.base : "-1"),
            "total": Number(offer.price.total),
            "currency": offer.price.currency
          }
        }
        offers.push(offer)
      }
      hotelEntitie.offers = offers
      await testSave(hotelEntitie);
    }

  }
};

function testSave(hotelEntitie) {
  return new Promise((resolve, reject) => {
    var hotel = new Hotel(hotelEntitie);
    hotel.save(function (err, savedEntity) {
      if (err != null) {
        console.log(err);
        reject({ error: "error", cause: err });
      }
      else {
        hotelEntitie.id = savedEntity.id;
        resolve(hotelEntitie);
      }
    });
  })
}

async function findHotelByCityCodes(ccde, checkInDate, checkOutDate, guests) {
  //console.log(ccde);
  let hotels = await amadeus.shopping.hotelOffers.get({
    cityCode: ccde,
    checkInDate: checkInDate,
    checkOutDate: checkOutDate,
    guests: guests


  })
  // console.log(hotels);
  return hotels;

}
module.exports = fillDatabaseWithHotelData;


