require('dotenv').config();

var Amadeus = require('amadeus');

var amadeus = new Amadeus({ 
    clientId: process.env.CLIENT_ID, 
    clientSecret: process.env.CLIENT_SECRET
  });
  module.exports = amadeus;
